<?php
namespace Game;

use Game\Helper\Suits;
use Game\Helper\Values;

/**
 * Class Card
 * @package Game
 */
class Card
{
	/** @var CardSuit $suit */
	private $suit;

	/** @var CardValue $value */
	private $value;

	/**
	 * Card constructor.
	 * @param CardSuit $suit
	 * @param CardValue $value
	 */
	public function __construct(CardSuit $suit, CardValue $value)
	{
		$this->suit = $suit;
		$this->value = $value;
	}

	/**
	 * @return \Game\CardSuit
	 */
	public function getSuit(): \Game\CardSuit
	{
		return $this->suit;
	}

	/**
	 * @param \Game\CardSuit $suit
	 */
	public function setSuit(\Game\CardSuit $suit): void
	{
		$this->suit = $suit;
	}

	/**
	 * @return CardValue
	 */
	public function getValue(): CardValue
	{
		return $this->value;
	}

	/**
	 * @param CardValue $value
	 */
	public function setValue(CardValue $value): void
	{
		$this->value = $value;
	}

	/**
	 * @param Card $card
	 * @return int
	 */
	public function getCardPoints()
	{
		switch (true) {
			case ($this->getSuit()->getName() === Suits::STR_HEARTS):
				return 1;
				break;
			case ($this->getSuit()->getName() === Suits::STR_CLUBS && $this->getValue()->getName() === Values::STR_JACK):
				return 2;
				break;
			case ($this->getSuit()->getName() === Suits::STR_SPADES && $this->getValue()->getName() === Values::STR_QUEEN):
				return 5;
				break;
			default:
				return 0;
		}

		return 0;
	}

	/**
	 * @return int
	 */
	public function getCardValue()
	{
		switch ($this->getValue()->getName()) {
			case Values::STR_ACE:
				return 14;
				break;
			case Values::STR_KING:
				return 13;
				break;
			case Values::STR_QUEEN:
				return 12;
				break;
			case Values::STR_JACK:
				return 11;
				break;
			case Values::STR_TEN:
				return 10;
				break;
			case Values::STR_NINE:
				return 9;
				break;
			case Values::STR_EIGHT:
				return 8;
				break;
			case Values::STR_SEVEN:
				return 7;
				break;
			default:
				return 0;
		}

		return 0;
	}
}