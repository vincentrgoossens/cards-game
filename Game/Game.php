<?php

namespace Game;

use Game\Helper\ArrayHelper;

class Game
{
	/** @var \DateTime $startDate */
	private $startDate;

	/** @var \DateTime $endDate */
	private $endDate;

	/** @var GameSettings $settings */
	private $settings;

	/** @var Player[] $players */
	private $players;

	/** @var Card[] $cards */
	private $cards;

	/** @var bool $gameOver */
	private $gameOver = false;

	/**
	 * Game constructor.
	 * @param GameSettings $settings
	 * @param array $players
	 */
	public function __construct(GameSettings $settings, array $players)
	{
		$this->settings = $settings;
		$this->players	= $players;
	}

	public function initialize()
	{
		/** @var CardValue $cardValue */
		foreach ($this->settings->getCardValues() as $cardValue) {
			/** @var CardSuit $cardSuit */
			foreach ($this->settings->getCardSuits() as $cardSuit) {
				$this->addCard(new Card($cardSuit, $cardValue));
			}
		}
	}

	/**
	 * @throws \Exception
	 */
	public function startGame()
	{
		// Register the startDate
		$this->startDate = new \DateTime();

		//Play until the game is not over
		while($this->gameOver === false){

			//Shuffle the cards
			$this->shuffleCards();

			//Give cards to players
			$this->giveCardsToPlayers();
			$this->printScreen(PHP_EOL.'Reshuffle cards.');

			foreach ($this->players as $key => $player) {
				$this->printScreen($player->getName() . ' has been dealt:');

				$playerCards = $player->getCards();
				foreach ($playerCards as $playerCard) {
					$this->printScreen($playerCard->getSuit()->getName().$playerCard->getValue()->getName(). ' ', false);
				}
			}


			// split the game into sets
			$gameSetChunks = array_chunk($this->cards, count($this->players));

			//lets play the sets
			foreach ($gameSetChunks as $setNumber=>$gameSetChunk) {
				if($this->gameOver){
					break;
				}
				$this->printScreen(PHP_EOL.'Round '.($setNumber+1));

				$gameSet = new GameSet();

				//Add Players to the set
				$gameSet->setPlayers($this->players);
				//shuffle($this->cards);

				//Shuffle Player to the first be random
				$gameSet->shufflePlayers();

				/** @var Player $gameSetPlayer */
				foreach ($gameSet->getPlayers() as $key=>$gameSetPlayer) {
					if($key ===0){
						$this->printScreen(': '.$gameSetPlayer->getName().'  starts the game', false);
					}
					$playedCard = $gameSet->addPlayedCardCard($gameSetPlayer, $gameSetPlayer->playCard($gameSet));
					$this->printScreen($gameSetPlayer->getName().' plays: '.$playedCard->getSuit()->getName().' '.$playedCard->getValue()->getName());
				}

				$highestCard = $gameSet->getSuitHighestCard();
				if(!$highestCard){
					continue;
				}
				foreach ($this->players as $key=>$player) {
					if ($player->getName() === $highestCard->getPlayer()->getName()) {
						//var_dump('Player lost '.$player->getName().' got points: '.$gameSet->getCardsPoints());
						$player->addPoints($gameSet->getCardsPoints());
						$this->printScreen($highestCard->getPlayer()->getName().' played '.$highestCard->getSuit()->getName().' '.$highestCard->getValue()->getName().', the highest matching card of this match and got '.$gameSet->getCardsPoints().' point added to his total
score. total score is '.$player->getPoints().' point.');

						if($player->getPoints() >= 50){
							$this->gameOver = true;
							$this->printScreen(PHP_EOL.$player->getName().' loses the game!');

							$this->printScreen('Points:');

							foreach ($this->players as $key=>$playerr) {
								$this->printScreen($playerr->getName().' : '.$playerr->getPoints());
							}

						}
					}
				}
			}
		};
	}

	/**
	 * @param string $text
	 * @param bool $breakLine
	 */
	private function printScreen(string $text, $breakLine = true)
	{
		if(getenv('GAME_SHOW_ECHO')){
			echo ($breakLine ? PHP_EOL :'').$text;
		}
	}

	private function shuffleCards()
	{
		shuffle($this->cards);
	}

	/**
	 *
	 */
	private function giveCardsToPlayers()
	{
		//Divide the deck cards into the numbers os players
		$cardsPlayers = ArrayHelper::partition($this->cards, count($this->players));
		foreach ($this->players as $key=>$player) {
			$player->setCards($cardsPlayers[$key]);
		}
	}

	/**
	 * @return GameSettings
	 */
	public function getSettings(): GameSettings
	{
		return $this->settings;
	}

	/**
	 * @param GameSettings $settings
	 */
	public function setSettings(GameSettings $settings): void
	{
		$this->settings = $settings;
	}

	/**
	 * @return Player[]
	 */
	public function getPlayers(): array
	{
		return $this->players;
	}

	/**
	 * @param Player[] $players
	 */
	public function setPlayers(array $players): void
	{
		$this->players = $players;
	}

	/**
	 * @return Card[]
	 */
	public function getCards(): array
	{
		return $this->cards;
	}

	/**
	 * @param Card[] $cards
	 */
	public function setCards(array $cards): void
	{
		$this->cards = $cards;
	}

	/**
	 * @param Card $card
	 */
	private function addCard(Card $card)
	{
		$this->cards[] = $card;
	}
}