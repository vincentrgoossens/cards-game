<?php

namespace Game;


class GameSet
{
	/** @var PlayedCard[] $cards */
	private $playedCards = [];

	/** @var Player[] $players */
	private $players;

	/**
	 * @return PlayedCard[]
	 */
	public function getPlayedCards(): array
	{
		return $this->playedCards;
	}

	/**
	 * @param PlayedCard[] $playedCards
	 */
	public function setPlayedCards(array $playedCards): void
	{
		$this->playedCards = $playedCards;
	}

	/**
	 * @param Player $player
	 * @param Card $card
	 */
	public function addPlayedCardCard(Player $player, PlayedCard $playedCard)
	{
		$this->playedCards[] = $playedCard;
		return $playedCard;
	}

	/**
	 * @return Player[]
	 */
	public function getPlayers(): array
	{
		return $this->players;
	}

	/**
	 * @param Player[] $players
	 */
	public function setPlayers(array $players): void
	{
		$this->players = $players;
	}

	public function shufflePlayers()
	{
		shuffle($this->players);
	}

	/**
	 * @return int
	 */
	public function getCardsPoints()
	{
		$points = 0;
		/** @var PlayedCard $playedCard */
		foreach ($this->playedCards as $playedCard){
			$points += $playedCard->getCardPoints();
		}
		return $points;
	}

	public function getFirstPlayedCard()
	{
		return (isset($this->playedCards[0]) ? $this->playedCards[0] : false );
	}

	/**
	 * @return bool|PlayedCard
	 */
	public function getSuitHighestCard()
	{
		$firstCard = $this->getFirstPlayedCard();

		if(!$firstCard){
			return false;
		}

		$playedCards = $this->getPlayedCards();
		unset($playedCards[0]);

		//Filter by the Suit first played
		/** @var PlayedCard[] $suitPlayedCards */
		$suitPlayedCards = array_reduce($playedCards, function($result, $item) use ($firstCard) {
			/** @var Card $item */
			if ($item->getSuit()->getName() === $firstCard->getSuit()->getName()) {
				$result[] = $item;
			}

			return $result;
		}, []);

		//get the highest card
		$highestCardValue = ['value'=> 0, 'playedCard' => null];
		foreach ($suitPlayedCards as $suitPlayedCard){
			if($suitPlayedCard->getCardValue() > $highestCardValue['value']){
				$highestCardValue['value'] = $suitPlayedCard->getCardValue();
				$highestCardValue['playedCard'] = $suitPlayedCard;
			}
		}

		return $highestCardValue['playedCard'];
	}
}