<?php

namespace Game;


use Game\Helper\Suits;
use Game\Helper\Values;

/**
 * Class GameSettings
 * @package Game
 */
class GameSettings
{
	/** @var int $numberOfCards */
	private $numberOfCards;

	/** @var CardValue[] $cardValues */
	private $cardValues;

	/** @var CardSuit[] $cardSuits */
	private $cardSuits;

	public function __construct(int $numberOfCards, array $cardValues, array $cardSuits)
	{
		$this->numberOfCards = $numberOfCards;
		$this->cardValues = $cardValues;
		$this->cardSuits = $cardSuits;
	}

	/**
	 * @return int
	 */
	public function getNumberOfCards(): int
	{
		return $this->numberOfCards;
	}

	/**
	 * @param int $numberOfCards
	 */
	public function setNumberOfCards(int $numberOfCards): void
	{
		$this->numberOfCards = $numberOfCards;
	}

	/**
	 * @return CardValue[]
	 */
	public function getCardValues(): array
	{
		return $this->cardValues;
	}

	/**
	 * @param CardValue[] $cardValues
	 */
	public function setCardValues(array $cardValues): void
	{
		$this->cardValues = $cardValues;
	}

	/**
	 * @return CardSuit[]
	 */
	public function getCardSuits(): array
	{
		return $this->cardSuits;
	}

	/**
	 * @param CardSuit[] $cardSuits
	 */
	public function setCardSuits(array $cardSuits): void
	{
		$this->cardSuits = $cardSuits;
	}
}