<?php
/**
 * Created by Global Ticket B.V. (globalticket.nl)
 * Date: 2020-02-02
 * Time: 23:02
 */

namespace Game\Helper;


class ArrayHelper
{
	public static function partition( $list, $p ) {
		$listlen = count( $list );
		$partlen = floor( $listlen / $p );
		$partrem = $listlen % $p;
		$partition = array();
		$mark = 0;
		for ($px = 0; $px < $p; $px++) {
			$incr = ($px < $partrem) ? $partlen + 1 : $partlen;
			$partition[$px] = array_slice( $list, $mark, $incr );
			$mark += $incr;
		}
		return $partition;
	}
}