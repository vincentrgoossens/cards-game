<?php

namespace Game\Helper;

class Suits
{
	CONST STR_CLUBS		= 'Clubs';
	CONST STR_DIAMONDS	= 'Diamonds';
	CONST STR_HEARTS	= 'Hearts';
	CONST STR_SPADES	= 'Spades';
}