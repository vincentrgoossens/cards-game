<?php

namespace Game\Helper;

class Values
{
	CONST STR_SEVEN = '7';
	CONST STR_EIGHT = '8';
	CONST STR_NINE = '9';
	CONST STR_TEN = '10';
	CONST STR_JACK = 'Jack';
	CONST STR_QUEEN = 'Queen';
	CONST STR_KING = 'King';
	CONST STR_ACE = 'Ace';
}