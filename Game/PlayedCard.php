<?php

namespace Game;

/**
 * Class PlayedCard
 * @package Game
 */
class PlayedCard extends Card
{
	/** @var Player $player */
	private $player;

	public function __construct(Player $player, Card $card)
	{
		$this->player = $player;
		$this->setSuit($card->getSuit());
		$this->setValue($card->getValue());
	}

	/**
	 * @return Player
	 */
	public function getPlayer(): Player
	{
		return $this->player;
	}

	/**
	 * @param Player $player
	 */
	public function setPlayer(Player $player): void
	{
		$this->player = $player;
	}
}