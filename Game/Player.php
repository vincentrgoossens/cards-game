<?php

namespace Game;

class Player
{
	/** @var string $name */
	private $name;

	/** @var Card[] $cards */
	private $cards;

	/** @var int $points */
	private $points = 0;

	public function __construct(string $name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name): void
	{
		$this->name = $name;
	}

	/**
	 * @return Card[]
	 */
	public function getCards(): array
	{
		return $this->cards;
	}

	/**
	 * @param Card[] $cards
	 */
	public function setCards(array $cards): void
	{
		$this->cards = $cards;
	}

	/**
	 * @param Card $card
	 */
	public function addCard(Card $card)
	{
		$this->cards[] = $card;
	}

	/**
	 * @return Card
	 */
	private function getRandomCard()
	{
		$card = array_rand($this->cards);
		$randomCard = $this->cards[$card];
		array_splice($this->cards, $card, 1 );
		return $randomCard;
	}

	/**
	 * Removes a random card form the player and return it
	 * @return Card
	 */
	private function playRandomCard()
	{
		$randomCard = $this->getRandomCard();
		return ($randomCard ? $randomCard :false );
	}

	private function playLowerCardFromTheSet()
	{
		$randomCard = $this->getRandomCard();
		return ($randomCard ? $randomCard :false );
	}

	/**
	 * @param GameSet $gameSet
	 * @return Card
	 */
	public function playCard(GameSet $gameSet)
	{
		if(empty($gameSet->getPlayedCards())){
			return new PlayedCard($this, $this->playRandomCard());
		}

		/** @var Card $firstPlayedCard */
		$firstPlayedCard = $gameSet->getPlayedCards()[0];
		$suitCards = $this->getSuitCards($firstPlayedCard->getSuit());

		ksort($suitCards);

		/** @var Card $lowerSuitCard */
		$lowerSuitCard = reset($suitCards);

		if(!$lowerSuitCard){
			return new PlayedCard($this, $this->playRandomCard());
		}

		//Remove the card from the player
		foreach ($this->getCards() as $key=>$card){
			if(($card->getSuit()->getName() === $lowerSuitCard->getSuit()->getName()) && ($card->getValue()->getName() === $lowerSuitCard->getValue()->getName())){
				unset($this->cards[$key]);
			}
		}

		return new PlayedCard($this, $lowerSuitCard);
	}


	/**
	 * @param CardSuit $cardSuit
	 * @return mixed
	 */
	private function getSuitCards(CardSuit $cardSuit)
	{
		$suitCards = array_reduce($this->getCards(), function($result, $item) use ($cardSuit) {
			/** @var Card $item */
			if ($item->getSuit()->getName() === $cardSuit->getName()) {
				$result[$item->getCardValue()] = $item;
			}

			return $result;
		}, []);

		return $suitCards;
	}

	/**
	 * @return int
	 */
	public function getPoints(): int
	{
		return $this->points;
	}

	/**
	 * @param int $points
	 */
	public function addPoints(int $points): void
	{
		$this->points += $points;
	}
}