<?php

require __DIR__.'/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__.'/../');
$dotenv->load();

use Game\Helper\Values as ValuesHelper;
use Game\Helper\Suits as SuitsHelper;
use Game\Player;

$gameSettings = new \Game\GameSettings(
	32,
	[
		new \Game\CardValue(ValuesHelper::STR_SEVEN),
		new \Game\CardValue(ValuesHelper::STR_EIGHT),
		new \Game\CardValue(ValuesHelper::STR_NINE),
		new \Game\CardValue(ValuesHelper::STR_TEN),
		new \Game\CardValue(ValuesHelper::STR_JACK),
		new \Game\CardValue(ValuesHelper::STR_QUEEN),
		new \Game\CardValue(ValuesHelper::STR_KING),
		new \Game\CardValue(ValuesHelper::STR_ACE),
	],
	[
		new \Game\CardSuit(SuitsHelper::STR_CLUBS),
		new \Game\CardSuit(SuitsHelper::STR_DIAMONDS),
		new \Game\CardSuit(SuitsHelper::STR_HEARTS),
		new \Game\CardSuit(SuitsHelper::STR_SPADES),
	]
);

$game = new \Game\Game(
	$gameSettings,
	[
		new Player('John'),
		new Player('Jane'),
		new Player('Jan'),
		new Player('Otto')
	]
);

$game->initialize();

$game->startGame();








