<?php

namespace tests;

use Game\Game;
use Game\GameSettings;
use Game\Player;
use PHPUnit\Framework\TestCase;
use Game\Helper\Values as ValuesHelper;
use Game\Helper\Suits as SuitsHelper;

class TestGame extends TestCase
{
	/** @var Game */
	protected $game;
	/** @var GameSettings $gameSettings */
	protected $gameSettings;

	protected function setUp(): void
	{
		$this->gameSettings = new \Game\GameSettings(
			32,
			[
				new \Game\CardValue(ValuesHelper::STR_SEVEN),
				new \Game\CardValue(ValuesHelper::STR_EIGHT),
				new \Game\CardValue(ValuesHelper::STR_NINE),
				new \Game\CardValue(ValuesHelper::STR_TEN),
				new \Game\CardValue(ValuesHelper::STR_JACK),
				new \Game\CardValue(ValuesHelper::STR_QUEEN),
				new \Game\CardValue(ValuesHelper::STR_KING),
				new \Game\CardValue(ValuesHelper::STR_ACE),
			],
			[
				new \Game\CardSuit(SuitsHelper::STR_CLUBS),
				new \Game\CardSuit(SuitsHelper::STR_DIAMONDS),
				new \Game\CardSuit(SuitsHelper::STR_HEARTS),
				new \Game\CardSuit(SuitsHelper::STR_SPADES),
			]
		);

		$this->game = new Game($this->gameSettings,
			[
				new Player('John'),
				new Player('Jane'),
				new Player('Jan'),
				new Player('Otto')
			]);
	}

	public function testNumberCards()
	{
		$this->assertGreaterThan(0, $this->gameSettings->getNumberOfCards());
	}

	public function testCardValues()
	{
		$this->assertIsArray($this->gameSettings->getCardValues());
	}

	public function testCardSuits()
	{
		$this->assertIsArray($this->gameSettings->getCardSuits());
	}
}